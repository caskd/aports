# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.258
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="maturin cargo py3-installer"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/charliermarsh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL="sparse"

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/completions/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
f94955fecd89b912b70f393ddc55b36feba22f22597bb061812261dab6ad7375775ed91acd196107bc20ae5a7d28764dd40cb67382171964f1912b9145318268  ruff-0.0.258.tar.gz
"
