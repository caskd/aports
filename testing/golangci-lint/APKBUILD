# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=golangci-lint
pkgver=1.52.0
pkgrel=0
pkgdesc="Fast linters runner for Go"
url="https://golangci-lint.run/"
arch="all"
license="GPL-3.0-or-later"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/golangci/golangci-lint/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local goldflags="
	-X main.version=$pkgver
	-X main.commit=AlpineLinux
	-X main.date=$(date -u "+%Y-%m-%dT%TZ" ${SOURCE_DATE_EPOCH:+-d @$SOURCE_DATE_EPOCH})
	"
	go build -v -o golangci-lint -ldflags "$goldflags" ./cmd/golangci-lint

	./golangci-lint completion bash > golangci-lint.bash
	./golangci-lint completion zsh > golangci-lint.zsh
	./golangci-lint completion fish > golangci-lint.fish
}

check() {
	# Testsuite enforces an old version of Go
	GOLANGCI_LINT_INSTALLED=true go test $(go list ./... | grep -v /test)
}

package() {
	install -Dm755 golangci-lint -t "$pkgdir"/usr/bin

	install -Dm644 golangci-lint.bash \
		"$pkgdir"/usr/share/bash-completion/completions/golangci-lint
	install -Dm644 golangci-lint.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_golangci-lint
	install -Dm644 golangci-lint.fish \
		"$pkgdir"/usr/share/fish/completions/golangci-lint.fish
}

sha512sums="
6278cf9cce7c833fe000062358f3ed3e6bb2ce7960438132aab49250673b533e9706cda439745022f0f0c2bdd9dbd9e7a04c27c050906c97dd41c5b43cfbaccc  golangci-lint-1.52.0.tar.gz
"
