# Contributor: Eric Molitor <eric@molitor.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=lld
pkgver=16.0.0
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="The LLVM Linker"
url="https://llvm.org/"
# cannot link anything and fails
# ld.lld: error: unknown emulation: elf64_s390
# ld.lld: error: src/gn/gn_main.o: could not infer e_machine
# from bitcode target triple s390x-alpine-linux-musl
# also fails hundreds of tests
arch="all !s390x"
license="Apache-2.0"
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="
	clang
	cmake
	compiler-rt
	libedit-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	llvm$_llvmver-test-utils
	llvm-libunwind-dev
	patchelf
	samurai
	zlib-dev
	"
checkdepends="gtest-dev bash llvm$_llvmver-test-utils"
subpackages="$pkgname-dbg $pkgname-libs $pkgname-dev $pkgname-doc"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/lld-${pkgver//_/}.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-${pkgver//_/-}/cmake-${pkgver//_/}.src.tar.xz
	"
builddir="$srcdir/$pkgname-${pkgver//_/}.src"

case "$CARCH" in
armhf)
	# for some reason they hang forever, but the actual linker works fine
	options="$options !check"
	;;
esac

prepare() {
	default_prepare
	mv "$srcdir"/cmake-${pkgver//_/}.src "$srcdir"/cmake
}

build() {
	CFLAGS="${CFLAGS/-g/-g1}" \
	CXXFLAGS="${CXXFLAGS/-g/-g1}" \
	CC=clang CXX=clang++ \
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DBUILD_SHARED_LIBS=ON \
		-DLLVM_INCLUDE_TESTS="$(want_check && echo ON || echo OFF)" \
		-DLLVM_EXTERNAL_LIT=/usr/bin/lit \
		-DLLD_BUILT_STANDALONE=ON
	cmake --build build
}

check() {
	ninja -C build check-lld
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 "$builddir"/docs/ld.lld.1 -t "$pkgdir"/usr/share/man/man1/

	case "$CARCH" in
	aarch64|arm*|x86|x86_64|ppc64le)
		# we have the scudo allocator on these arches, so link lld to it to
		# significantly improve performance, especially in LTO contexts
		patchelf --add-needed libscudo.so "$pkgdir"/usr/bin/lld
		;;
	esac
}

sha512sums="
40eb83b4ef4f95704788e37cd3fa7b06dcbc197ba915b359eda7d219de5092c8c44f883509a926b97027c299c2171066b2a1519d36a5642474bfca3f88498392  lld-16.0.0.src.tar.xz
4f21461aa8165061dbea47dcda4f098957e16bd307484bcb66884cf5a0776197f69a74002d5601229c4630db53ac44049f3f2ce1e96a6bb16ba3df828d387932  cmake-16.0.0.src.tar.xz
"
