# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtquicktimeline
pkgver=6.4.3
pkgrel=0
pkgdesc="Qt module for keyframe-based timeline construction"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	vulkan-headers
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
options="!check" # No tests
builddir="$srcdir/qtquicktimeline-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtquicktimeline-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8310e0b360859e59157b5968647a5bb82bfd09f2f61c1b289669e551781e8fd1285f3efb8ed1324013ad14b192f0d9963c8584cfec98a136c82f5956ac1de9ae  qtquicktimeline-everywhere-src-6.4.3.tar.xz
"
