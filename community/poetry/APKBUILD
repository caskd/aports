# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=poetry
pkgver=1.4.1
pkgrel=0
pkgdesc="Python3 dependency management and packaging system"
url="https://python-poetry.org"
arch="noarch"
license="MIT"
depends="
	python3
	py3-poetry-core
	py3-poetry-plugin-export
	py3-build
	py3-cachecontrol
	py3-cleo
	py3-crashtest
	py3-dulwich
	py3-filelock
	py3-html5lib
	py3-installer
	py3-jsonschema
	py3-keyring
	py3-lockfile
	py3-packaging
	py3-pexpect
	py3-pkginfo
	py3-platformdirs
	py3-pyproject-hooks
	py3-requests
	py3-requests-toolbelt
	py3-shellingham
	py3-tomlkit
	py3-trove-classifiers
	py3-virtualenv
	py3-urllib3
	"
makedepends="py3-gpep517 py3-installer"
checkdepends="py3-pytest py3-pytest-mock py3-deepdiff py3-httpretty"
source="$pkgname-$pkgver.tar.gz::https://github.com/python-poetry/poetry/archive/$pkgver.tar.gz"
options="!check" # TODO: enable tests

prepare() {
	default_prepare

	# flatdict module is not packaged in aports
	rm tests/config/test_config.py
}

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
cfb2a8b07b2187af7b6974b863beff0158dfbb40b2a190285e8307cd9ce30a8eec45d4acaf3b8d6810adee65e913935b185f14243c076037a78aa8e5cc469701  poetry-1.4.1.tar.gz
"
