# Contributor: Jiri Kastner <cz172638@gmail.com>
# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-jaraco.collections
pkgver=3.9.0
pkgrel=0
pkgdesc="jaraco - Module for text manipulation"
url="https://github.com/jaraco/jaraco.collections"
arch="noarch"
license="MIT"
depends="py3-jaraco.classes py3-jaraco.text"
# py3-setuptools_scm is needed to set python module version
makedepends="py3-gpep517 py3-setuptools py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest"
source="https://files.pythonhosted.org/packages/source/j/jaraco.collections/jaraco.collections-$pkgver.tar.gz"
builddir="$srcdir/jaraco.collections-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/jaraco.collections-$pkgver-py3-none-any.whl
}

sha512sums="
cac756866dffee2a7b41d4cc3a957e56bd7353454c0a819e94d7b5102bde624a759e84e74eccd6df475b1753f8730f72ac4dd2fc464085a387c879a0e2ac1891  jaraco.collections-3.9.0.tar.gz
"
